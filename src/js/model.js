import {
  API_URL,
  RECIPES_RESULT_PER_PAGE,
  RECIPES_DEFAULT_PAGE,
  BOOKSMARKS,
  WRONG_INGREDIENT_FORMAT_MESSAGE,
  API_KEY as KEY,
} from './config';
import { request } from './helpers';
//import { getJson, sendJson } from './helpers';

// private function
const formatRecipe = recipe => {
  return {
    _id: recipe.id,
    title: recipe.title,
    publisher: recipe.publisher,
    servings: recipe.servings,
    ingredients: recipe.ingredients,
    cookingTime: recipe.cooking_time,
    sourceUrl: recipe.source_url,
    imageUrl: recipe.image_url,
    bookmarked: state.bookmarks.has(recipe.id), //!!value or Boolean(value) is used to convert value in boolean
    ...(recipe.createdAt && {
      createdAt: recipe.createdAt,
    }),
    ...(recipe.key && {
      key: recipe.key,
    }) /* it's a trick to add property if it's exist on the parent in this case we check if recipe has 'key' property:
      - if false then we return undefined we is not kept in the object - - but if true we create and return an object whith the propety   name and it's value next this object is spread in the object as key : value (so the property is added) :)
     */,
  };
};
const formatPreviewRecipe = recipe => {
  return {
    _id: recipe.id,
    title: recipe.title,
    publisher: recipe.publisher,
    imageUrl: recipe.image_url,
    ...(recipe.key && {
      key: recipe.key,
    }),
  };
};
const formatApiRecipe = (rawData, ingredients) => {
  return {
    title: rawData.title,
    publisher: rawData.publisher,
    servings: +rawData.servings,
    cooking_time: +rawData.cookingTime,
    source_url: rawData.sourceUrl,
    image_url: rawData.image,
    ingredients,
  };
};
const persitBookmark = bookmarks => {
  localStorage.setItem(BOOKSMARKS, JSON.stringify([...bookmarks]));
};
const loadBookmark = () => {
  const stored = localStorage.getItem(BOOKSMARKS);
  const bookmarks = stored ? JSON.parse(stored) : [];
  return new Map(bookmarks);
};
const generateIngredients = rawObject =>
  Object.entries(rawObject)
    .filter(([name, value]) => name.startsWith('ingredient-') && value)
    .map(([_, val]) => {
      const ingrs = val.trim().split(',');
      if (3 !== ingrs.length) throw new Error(WRONG_INGREDIENT_FORMAT_MESSAGE);
      const [quantity, unit, description] = ingrs;
      return { quantity: +quantity || null, unit, description };
    });

//State
export const state = {
  recipe: {},
  search: {
    query: '',
    results: [],
    resultPerPage: RECIPES_RESULT_PER_PAGE,
    currentPage: RECIPES_DEFAULT_PAGE,
  },
  bookmarks: loadBookmark(), //better to use an set
};

//API
export const loadRecipe = async function (id) {
  try {
    const { recipe } = await request(`${API_URL}${id}?key=${KEY}`);
    state.recipe = formatRecipe(recipe);
  } catch (err) {
    console.error(`${err} 🔥🧨`);
    throw err; //we rethrow err because we want to handle it in the centralized place in the controller
  }
};

export const searchRecipes = async function (query) {
  try {
    state.search.query = query;
    const { recipes } = await request(`${API_URL}?search=${query}&key=${KEY}`);
    const data = recipes.map(r => formatPreviewRecipe(r));
    state.search.results = data;
    state.search.currentPage = RECIPES_DEFAULT_PAGE;
  } catch (err) {
    console.error(`${err} 🔥🧨`);
    throw err; //we rethrow err because we want to handle it in the centralized place in the controller
  }
};

export const getRecipesPaginated = function (page = state.search.currentPage) {
  const start = (page - 1) * state.search.resultPerPage;
  const end = page * state.search.resultPerPage;
  return state.search.results.slice(start, end);
};

export const updateServings = function (newServings) {
  state.recipe.ingredients.forEach(ing => {
    ing.quantity = (ing.quantity * newServings) / state.recipe.servings;
  });

  state.recipe.servings = newServings;
};

export const toogleBookmark = function (recipe) {
  //add recipe to bookmarks
  if (!recipe._id) return;
  state.bookmarks.has(recipe._id)
    ? state.bookmarks.delete(recipe._id)
    : state.bookmarks.set(recipe._id, recipe);
  //store the bookmark in localstorage
  persitBookmark(state.bookmarks);
  //if recipe is current recipe we set it has bookmarked
  if (recipe === state.recipe)
    state.recipe.bookmarked = !state.recipe.bookmarked;
};

export const getBookmarks = function () {
  return [...state.bookmarks].map(([_, recipe]) => recipe);
};

export const saveRecipe = async function (data) {
  try {
    const ingredients = generateIngredients(data);
    const recipeData = formatApiRecipe(data, ingredients);
    const { recipe } = await request(`${API_URL}?key=${KEY}`, {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(recipeData),
    });
    //key, createdAt,
    state.recipe = formatRecipe(recipe);
    toogleBookmark(state.recipe);
  } catch (err) {
    throw err;
  }
};

/*export const findBookmarkedRecipe = id =>
  [...state.bookmarks].find(bookmarked => id === bookmarked);*/
