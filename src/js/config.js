export const API_URL = 'https://forkify-api.herokuapp.com/api/v2/recipes/';

export const PROMISE_TIMEOUT = 5;
export const RECIPES_RESULT_PER_PAGE = 10;
export const RECIPES_DEFAULT_PAGE = 1;
export const NOT_FOUND_RECIPE_MSG =
  'We could not find that recipe. Please try another one!';
export const NOT_FOUND_RECIPES_MSG =
  'No recipes found found for your query [__query]!. Please try again ;)';
export const NOT_FOUND_BOOKMARKS_MSG =
  'No bookmarks yet. Find a nice recipe and bookmark it :)';
export const WRONG_INGREDIENT_FORMAT_MESSAGE =
  'Wrong ingredient format! Please use the correct format :(';
export const SUCCESS_UPLOAD_MESSAGE = 'Recipe was successfully uploaded :)';
export const MESSAGE = '';
export const SERVING_MINUS = 'minus';
export const BOOKSMARKS = 'bookmarks';
export const MODAL_CLOSE_SEC = 2.5;
export const API_KEY = 'a72a962c-0fbd-4d2e-9023-ea6c1c5462af';
//twitter --> Jonas Schmedtmann
