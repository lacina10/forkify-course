import { PROMISE_TIMEOUT as sec } from './config';

const timeout = function (s) {
  return new Promise(function (_, reject) {
    setTimeout(function () {
      reject(new Error(`Request took too long! Timeout after ${s} second`));
    }, s * 1000);
  });
};

export const request = async function (url, options = undefined) {
  try {
    const promise = options ? fetch(url, options) : fetch(url);
    const res = await Promise.race([promise, timeout(sec)]);
    const data = await res.json();
    //handle error
    if (!res.ok) throw new Error(`Erreur: ${res.status} : ${data.message}`);
    return data.data;
  } catch (err) {
    //console.error(`${err} in helper`);
    throw err; //we rethrow err because we want to handle it in the centralized place in the model
  }
};

/*
export const getJson = async function (url) {
  try {
    const res = await Promise.race([fetch(url), timeout(sec)]);
    const data = await res.json();
    //handle error
    if (!res.ok) throw new Error(`Erreur: ${res.status} : ${data.message}`);
    return data.data;
  } catch (err) {
    //console.error(`${err} in helper`);
    throw err; //we rethrow err because we want to handle it in the centralized place in the model
  }
};

export const sendJson = async function (
  url,
  body,
  options = { method: 'POST', headers: { 'Content-Type': 'application/json' } }
) {
  try {
    options.body = JSON.stringify(body);
    const res = await Promise.race([fetch(url, options), timeout(sec)]);
    const data = await res.json();
    //handle error
    if (!res.ok) throw new Error(`Erreur: ${res.status} : ${data.message}`);
    return data.data;
  } catch (err) {
    //console.error(`${err} in helper`);
    throw err; //we rethrow err because we want to handle it in the centralized place in the model
  }
}; 
*/
