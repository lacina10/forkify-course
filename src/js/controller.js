import 'core-js/stable';
import 'regenerator-runtime/runtime';

import * as model from './model.js';
import { MODAL_CLOSE_SEC } from './config';
import recipeView from './views/recipeView.js';
import searchView from './views/searchView.js';
import recipesView from './views/recipesView.js';
import paginationView from './views/paginationView.js';
import bookmarkView from './views/bookmarkView.js';
import recipeFormView from './views/form/recipeFormView.js';

if (module.hot) {
  // it's comming from parcel to maintain the previous state
  module.hot.accept();
}

const processRecipesPagination = (page, isNewSearch = true) => {
  const results = model.getRecipesPaginated(page);
  true === isNewSearch
    ? recipesView.render(results)
    : recipesView.update(results);
  //render initial pagination
  model.state.search.currentPage = page;
  paginationView.render({ ...model.state.search });
};

const controllerRecipe = async function () {
  try {
    //get hash
    let { hash: id } = window.location;
    //guard for empty id
    if (!id) return;
    //render the loader
    recipeView.renderSpinner();
    //1 - update results view to mark selected search result
    model.state.search.results.length &&
      processRecipesPagination(model.state.search.currentPage);
    model.state.bookmarks.size
      ? bookmarkView.render(model.getBookmarks())
      : bookmarkView.update(model.getBookmarks());
    //get recipes
    await model.loadRecipe(id.replace('#', ''));
    const { recipe } = model.state;
    //Render recipe
    recipeView.render(recipe);
  } catch (err) {
    console.error(err);
    recipeView.renderError();
  }
};

const controllerSearchRecipe = async function () {
  try {
    const query = searchView.getSearchQuery();
    if (!query) return;
    //render the loader
    recipesView.renderSpinner();
    await model.searchRecipes(query);
    //const { results } = model.state.search;
    processRecipesPagination(model.state.search.currentPage, true);
  } catch (err) {
    console.log(err);
    recipeView.renderError('Search error message');
  }
};

const controllerPagination = function (page) {
  processRecipesPagination(page);
};

const controllerServings = function (servings) {
  if (1 > servings) return;
  // update the recipe servings (in state)
  model.updateServings(servings);
  // update the recipe view (rerender the recipe view)
  const { recipe } = model.state;
  //recipeView.render(recipe);
  recipeView.update(recipe);
};

const controllerAddBookmark = function () {
  model.toogleBookmark(model.state.recipe);
  recipeView.update(model.state.recipe);
  bookmarkView.render(model.getBookmarks());
  //console.log(model.getBookmarks());
};

const controllerBookmark = function () {
  bookmarkView.render(model.getBookmarks());
};

const controllerSaveRecipe = async function (formData) {
  try {
    recipeFormView.renderSpinner();
    await model.saveRecipe(formData);
    //render recipe
    recipeView.render(model.state.recipe);
    //render bookmark
    bookmarkView.render(model.getBookmarks());
    //display success message
    recipeFormView.renderMessage();
    //change the id in the url without reload the page
    window.history.pushState(null, '', `#${model.state.recipe._id}`); // pushState wait for 3 arguments state, title and url or the hash
    //window.history.back() --> go back to the last page
    //close the window form after some sec
    setTimeout(function () {
      recipeFormView.triggerToogleForm();
    }, MODAL_CLOSE_SEC * 1000);
    console.log(model.state.recipe);
  } catch (err) {
    recipeFormView.renderError(err.message);
  }
};
//routing
const init = () => {
  bookmarkView.addHandlerBookmark(controllerBookmark);
  recipeView.addHandlerRender(controllerRecipe);
  recipeView.addHandlerServings(controllerServings);
  recipeView.addHandlerAddBookmark(controllerAddBookmark);
  searchView.addHandlerSearch(controllerSearchRecipe);
  paginationView.addHandlerPagination(controllerPagination);
  recipeFormView.addHandlerSaveRecipe(controllerSaveRecipe);
};

init();
///////////////////////////////////////

console.log('Build is working now!');
