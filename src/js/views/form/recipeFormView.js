import View from '../View';
import { SUCCESS_UPLOAD_MESSAGE } from '../../config';

class recipeFormView extends View {
  _message = SUCCESS_UPLOAD_MESSAGE;
  _overlay = document.querySelector('div.overlay');
  _window = this._overlay.parentElement.querySelector('div.add-recipe-window');
  _parentElement = this._window.querySelector('form.upload');
  _closeRecipeFormBtn = this._window.querySelector('button.btn--close-modal');
  _addRecipeFormBtn = document.querySelector('button.nav__btn--add-recipe');

  constructor() {
    super();
    this._addHandlerDisplayForm();
    this._HandlerHideForm();
  }

  _addHandlerDisplayForm() {
    //we arrow form have the this set to the class and not on the elem btn
    this._addRecipeFormBtn.addEventListener('click', e => {
      this.toogleRecipeForm([this._overlay, this._window]);
    });
  }

  _HandlerHideForm() {
    //we arrow form have the this set to the class and not on the elem btn
    [this._overlay, this._closeRecipeFormBtn].forEach(elm =>
      elm.addEventListener('click', e => {
        this.toogleRecipeForm([this._overlay, this._window]);
      })
    );
  }

  toogleRecipeForm(elements) {
    elements.forEach(element => element.classList.toggle('hidden'));
  }

  triggerToogleForm() {
    this.toogleRecipeForm([this._overlay, this._window]);
    //for trigger an event we can use elem.event() -> ex: this.overlay.click() or this._overlay.fireEvent(`on${e.evenType}`, e); for event like mousedown|change etc
  }

  addHandlerSaveRecipe(handler) {
    this._parentElement.addEventListener('submit', function (e) {
      e.preventDefault();
      const formDataArr = [...new FormData(this)]; // get an array of array where the first item is the elem name and the second the elem value
      const data = Object.fromEntries(formDataArr); //take an array of array an convert it to an object so the opposite of entries method
      //console.log(new FormData(this));
      handler(data);
    });
  }

  _generateMarkup(recipes) {}
}

export default new recipeFormView();
