import { NOT_FOUND_RECIPES_MSG } from '../config';
import View from './View';
import previewView from './previewView';

class RecipesView extends View {
  _parentElement = document.querySelector('ul.results');
  _errorMessage = NOT_FOUND_RECIPES_MSG;

  _generateMarkup(recipes) {
    return previewView.render(recipes, false);
  }
}

export default new RecipesView();
