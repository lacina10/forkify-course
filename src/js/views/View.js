import icons from '../../img/icons.svg';

export default class View {
  _data;

  _clear() {
    this._parentElement.innerHTML = '';
  }

  _insertMarkup(markup, parent = this._parentElement) {
    this._clear();
    parent.insertAdjacentHTML('beforeend', markup);
  }
  /**
   * Render the received object to the DOM
   * @param {Object | Object[]} data the data to be rendered (eg: recipe)
   * @param {boolean} [render=true] if false, create markup string instead of rendering to the DOM
   * @returns {undefined | string} A markup is return if render is false
   * @this {Object} View instance
   * @author Lacina DIANE
   * @todo finish the implementation
   */
  //the [] means the parameter are optionas
  render(data, render = true) {
    if (!data || (Array.isArray(data) && 0 === data.length))
      return this.renderError();
    this._data = data;
    const markup = this._generateMarkup(this._data);
    //return the generated markup if we don't want to render
    if (!render) return markup;
    //insert HTML code if render is true
    this._insertMarkup(markup);
  }

  update(data) {
    if (!data || (Array.isArray(data) && 0 === data.length))
      return this.renderError();
    this._data = data;
    const prototypeMarkup = this._generateMarkup(this._data);

    //we create a virtual DOM fragment based on generated markup
    const virtualDOMFragment = document
      .createRange()
      .createContextualFragment(prototypeMarkup);
    // we get all elements of this virtual DOM in an array
    const virtualDOMElelements = [...virtualDOMFragment.querySelectorAll('*')];
    //get all elements of the same container in the current DOM
    const currentDOMElements = [...this._parentElement.querySelectorAll('*')];
    //search for diff between current and virtual DOM
    virtualDOMElelements.forEach((virtualElm, i) => {
      //isEqualNode check if two node has recusivelly the same structure, contents and attributes and we know that text content is always the first nodechild of a node and that nodeValue alway return null if the node has no text value
      if (
        !virtualElm.isEqualNode(currentDOMElements[i]) &&
        '' !== virtualElm.firstChild?.nodeValue.trim()
      ) {
        // we update the changed textcontent
        currentDOMElements[i].textContent = virtualElm.textContent;
      }
      //we update the current DOM attributes
      if (!virtualElm.isEqualNode(currentDOMElements[i])) {
        [...virtualElm.attributes].forEach(attr =>
          currentDOMElements[i].setAttribute(attr.name, attr.value)
        );
      }
    });
    //console.log(currentDOMElements, virtualDOMElelements);
  }

  renderSpinner() {
    const markup = `<div class="spinner">
          <svg>
            <use href="${icons}.svg#icon-loader"></use>
          </svg>
        </div>`;
    this._insertMarkup(markup);
  }
  renderError(message = this._errorMessage) {
    const markup = `<div class="error">
            <div>
              <svg>
                <use href="${icons}#icon-alert-triangle"></use>
              </svg>
            </div>
            <p>${message}</p>
          </div>`;
    this._insertMarkup(markup);
  }
  renderMessage(message = this._message) {
    const markup = `<div class="message">
            <div>
              <svg>
                <use href="${icons}#icon-smile"></use>
              </svg>
            </div>
            <p>${message}</p>
          </div>`;

    /*if(['string', 'object'].includes(typeof replacement) && 0 < replacement.length){
        replacement = 'string' === typeof replacement ? [replacement] : replacement
    }*/
    this._insertMarkup(markup);
  }
}
