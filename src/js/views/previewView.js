import icons from '../../img/icons.svg';
import View from './View';

class PreviewView extends View {
  _generateMarkup(recipes) {
    const id = window.location.hash.slice(1);
    return recipes.reduce((acc, recipe) => {
      return `${acc}<li class="preview">
            <a class="preview__link ${
              id === recipe._id ? 'preview__link--active' : ''
            }" href="#${recipe._id}"> 
              <figure class="preview__fig">
                <img src="${recipe.imageUrl}" alt="${recipe.title}" />
              </figure>
              <div class="preview__data">
                <h4 class="preview__title">${recipe.title}</h4>
                <p class="preview__publisher">${recipe.publisher}</p>
                <div class="preview__user-generated ${
                  recipe.key ? '' : 'hidden'
                }">
                  <svg>
                    <use href="${icons}#icon-user"></use>
                  </svg>
                </div> 
              </div>
            </a>
          </li>`;
    }, '');
  }
}

export default new PreviewView();
