import { NOT_FOUND_BOOKMARKS_MSG } from '../config';
import View from './View';
import previewView from './previewView';

class BookmarkView extends View {
  _parentElement = document.querySelector('ul.bookmarks__list');
  _errorMessage = NOT_FOUND_BOOKMARKS_MSG;

  _generateMarkup(recipes) {
    return previewView.render(recipes, false);
  }

  addHandlerBookmark(handler) {
    window.addEventListener('load', handler);
  }
}

export default new BookmarkView();
