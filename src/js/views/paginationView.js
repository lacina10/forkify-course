import icons from '../../img/icons.svg';
import View from './View';

class PaginationView extends View {
  _parentElement = document.querySelector('div.pagination');

  _generateMarkup(search) {
    const nbrPages = Math.ceil(search.results.length / search.resultPerPage);
    // page 1, and there are other page
    if (1 === search.currentPage && 1 < nbrPages) {
      return this._generateButtonMarkup(
        'right',
        'next',
        search.currentPage + 1
      );
    }
    // last page
    if (1 < search.currentPage && search.currentPage === nbrPages) {
      return this._generateButtonMarkup('left', 'prev', nbrPages - 1);
    }
    // other page
    if (search.currentPage < nbrPages) {
      return `${this._generateButtonMarkup(
        'left',
        'prev',
        search.currentPage - 1
      )}${this._generateButtonMarkup('right', 'next', search.currentPage + 1)}`;
    }
    // page 1, and there are NO other result
    return '';

    // intermediare page
  }
  _generateButtonMarkup(place, sibblings, page) {
    return `<button data-goto=${page} class="btn--inline pagination__btn--${sibblings}">
            <svg class="search__icon">
              <use href="${icons}#icon-arrow-${place}"></use>
            </svg>
            <span>Page ${page}</span>
          </button>`;
  }
  addHandlerPagination(handler) {
    this._parentElement.addEventListener('click', function (e) {
      const btn = e.target.closest('button.btn--inline');
      if (!btn.matches('button.btn--inline')) return;
      const goto = +btn.dataset.goto;
      handler(goto);
    });
  }
}
export default new PaginationView();
