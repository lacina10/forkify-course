class SearchView {
  _parentElement = document.querySelector('form.search');

  getSearchQuery() {
    const query = this._parentElement.querySelector(
      'input.search__field'
    ).value;
    this.clearInput();
    return query;
  }

  clearInput() {
    this._parentElement.querySelector('input.search__field').value = '';
  }

  addHandlerSearch(handler) {
    this._parentElement.addEventListener('submit', function (e) {
      e.preventDefault();
      handler();
    });
  }
}

export default new SearchView();
